﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OrderService.EntityModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrderService.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        [HttpGet("GetService")]
        public string GetService()
        {
            return "Order service is running";
        }

        [HttpGet("{Id}")]
        public ActionResult<Order> GetOrder(int Id)
        {
            OrderDbContext ctx = new OrderDbContext();
            Order order = ctx.Orders.Where(x => x.Id == Id).FirstOrDefault();
            return order;
        }


    }
}
