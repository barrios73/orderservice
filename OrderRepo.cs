﻿using OrderService.EntityModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrderService
{
    
    public class OrderRepo
    {
        //private readonly OrderDbContext _ctx = new OrderDbContext();

        //public OrderRepo(OrderDbContext ctx)
        //{
        //    _ctx = ctx;
        //}


        public Order SaveOrder(Order order)
        {
            OrderDbContext _ctx = new OrderDbContext();
            _ctx.Add(order);
            _ctx.SaveChanges();
            return order;
        }

    }
}
