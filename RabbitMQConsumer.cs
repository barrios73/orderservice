﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using OrderService.EntityModel;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace OrderService
{
    public class RabbitMQConsumer: BackgroundService
    {
        private readonly ILogger<RabbitMQConsumer> _logger;
        private ConnectionFactory _connectionFactory;
        private IConnection _connection;
        private IModel _channel;
        private readonly OrderRepo _orderRepo;
        private readonly OrderSvc _orderSvc;

        public RabbitMQConsumer(ILogger<RabbitMQConsumer> logger, OrderRepo orderRepo,
            OrderSvc orderSvc)
        {
            _logger = logger;
            _orderRepo = orderRepo;
            _orderSvc = orderSvc;
        }

        public override Task StartAsync(CancellationToken cancellationToken)
        {
            _connectionFactory = new ConnectionFactory
            {                
                HostName = Environment.GetEnvironmentVariable("RABBITMQ_HOST"),
                Port = Convert.ToInt32(Environment.GetEnvironmentVariable("RABBITMQ_PORT"))
            };

            try
            {
                _connection = _connectionFactory.CreateConnection();

                _logger.LogInformation("RabbitMQ connection is opened.");

                _channel = _connection.CreateModel();

                _channel.ExchangeDeclare(exchange: "cart",
                            type: ExchangeType.Direct,
                            durable: true,
                            autoDelete: false);


                _channel.QueueDeclare(queue: "orders",
                    durable: true,
                    exclusive: false,
                    autoDelete: false,
                    arguments: null);


                _channel.QueueBind(queue: "orders",
                                   exchange: "cart",
                                   routingKey: "INITIATED");

                //add other queue and routing key for other events

            }
            catch (Exception ex)
            {
                _logger.LogError("OrderSvc, Error in starting RabbitMQ background svc - " + ex.Message);
            }
            //_channel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);

            return base.StartAsync(cancellationToken);
        }


        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            var consumer = new EventingBasicConsumer(_channel);
            consumer.Received += (sender, e) =>
            {
                var body = e.Body.ToArray();
                var message = Encoding.UTF8.GetString(body);

               
                    Order msg = JsonSerializer.Deserialize<Order>(message);
                    _orderRepo.SaveOrder(msg);
                    _orderSvc.ProcessOrder(msg);
                    

            };

            _channel.BasicConsume("orders", true, consumer);
            await Task.CompletedTask;
        }


        public override async Task StopAsync(CancellationToken cancellationToken)
        {
            await base.StopAsync(cancellationToken);
            _connection.Close();
            _logger.LogInformation("RabbitMQ connection is closed.");
        }
    }
}
