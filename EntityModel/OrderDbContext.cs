﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrderService.EntityModel
{
    public partial class OrderDbContext: DbContext
    {
        public OrderDbContext()
        {
        }

        public OrderDbContext(DbContextOptions<OrderDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Order> Orders { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                string server = Environment.GetEnvironmentVariable("MSSQLSERVER");
                string port = Environment.GetEnvironmentVariable("MSSQLPORT");
                string db = Environment.GetEnvironmentVariable("DB");
                string userid = Environment.GetEnvironmentVariable("USERID");
                string password = Environment.GetEnvironmentVariable("PASSWORD");

                //string server = "tcp:192.168.137.1";
                //string port = "1433";
                //string userid = "InSitu";
                //string password = "insituanomaly";
                //string db = "Order";


#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                //optionsBuilder.UseSqlServer("Data Source = tcp:192.168.137.1, 1433; Initial Catalog = Order; User ID = InSitu; Password = insituanomaly");

                optionsBuilder.UseInMemoryDatabase("OrderDb");

                //optionsBuilder.UseSqlServer("Data Source =" + server + ", " + port + "; " +
                //    "Initial Catalog = " + db + ";" +
                //    "User ID = " + userid + ";" +
                //    "Password = " + password
                //    );

            }
        }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<Order>(entity =>
            {
                entity.ToTable("Order");

                entity.Property(e => e.Id);

                entity.Property(e => e.CartId);

                entity.Property(e => e.OrderId);
                entity.Property(e => e.OrderStatus);
                entity.Property(e => e.ProductId);
                entity.Property(e => e.ProductName);
                entity.Property(e => e.Price);
                entity.Property(e => e.Total);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);


    }
}
