﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrderService.EntityModel
{
    public class Order
    {
        public int Id { get; set; }
        public int CartId { get; set; }
        public int OrderId { get; set; }
        public string OrderStatus { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public decimal Price { get; set; }
        public int Total { get; set; }
    }
}
