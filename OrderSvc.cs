﻿using OrderService.EntityModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrderService
{
    public class OrderSvc
    {
        private readonly RabbitMQPublisher _publisher;

        public OrderSvc(RabbitMQPublisher publisher)
        {
            _publisher = publisher;
        }

        public void ProcessOrder(Order order)
        {
            
            if (order.Id < 10)
            {
                order.OrderStatus = "FAILED";
                _publisher.Publish(order, "cart", "FAILED", "order-processed");

            }
            else
            {
                order.OrderStatus = "SUCCESS";
                _publisher.Publish(order, "cart", "SUCCESS", "order-processed");

            }
        }
    }
}
